import React, { Fragment } from 'react';
import { Route, Switch } from 'react-router';
import Homepage from './pages/homepage/homepage.component';
import PizzaPage from './pages/pizzapage/pizzapage.component';


const App = () => (
    <Fragment>
        <Switch>
            <Route exact path='/' component={Homepage}/>
            <Route exact path='/pizza/:idPizza' component={PizzaPage}/>
        </Switch>
    </Fragment>
  ) ;
  
  export default App;
  