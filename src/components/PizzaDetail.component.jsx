import { Button, Grid, Paper } from '@material-ui/core';
import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';

const PizzaDetail = ({pizza}) => {


    return (
        <Fragment>

            <Grid container justify='center'>
                <h2>{pizza.nombre}</h2>
            </Grid>

            <Grid container justify='center'>
                <Paper className='paper'>
                    <img src={`../../assets/img/${pizza.img}`} alt={pizza.nombre} />
                </Paper>
                <Paper className='paper'>
                    <h4>Tamaños</h4>
                    <ul>
                        {
                            pizza.tamagnos.map((tamagno, index) => {
                                return (
                                    <li key={index}>&nbsp;&nbsp; {tamagno} </li>
                                )
                            })

                        }
                    </ul>
                    <br />
                    <br />
                    <h4>Ingredientes</h4>
                    <ul>
                        {
                            pizza.ingredientes.map((ingrediente, index) => {
                                return (
                                    <li key={index}>&nbsp;&nbsp; {ingrediente} </li>
                                )
                            })

                        }
                    </ul>

                    <br />
                    <br />

                    <Button component={Link}
                        to="/"
                        variant="contained"
                        color="secondary"
                        className='colorVolver'
                    >
                        Volver
                    </Button>

                </Paper>
            </Grid>
        </Fragment>
    )
}

export default PizzaDetail;