import React from 'react'

const Footer = () =>  (
    <footer>
        <p>&copy; {new Date().getFullYear()} - Felipe Monti</p>
    </footer>
)

export default Footer;
