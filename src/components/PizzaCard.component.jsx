import { Button, Paper } from '@material-ui/core';
import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';

const PizzaCard = ({id, nombre, img}) => {
    return (
        <Fragment>
            <Paper className='paper'>
                <h2>{nombre}</h2>
                <img src={img} alt={nombre} />
                
                <Button component={Link}  to={`/pizza/${id}`} variant="contained"
                        color="primary" className='colorDetalle'> Ver Detalles </Button>
            </Paper> 
        </Fragment>
    )
}

export default PizzaCard;