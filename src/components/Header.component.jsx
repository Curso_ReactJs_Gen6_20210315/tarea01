import { AppBar, Toolbar } from '@material-ui/core';
import React from 'react';

const Header = () => (

    <AppBar position='sticky'>
         <Toolbar>
             <h1>PIZZAS APP</h1>
         </Toolbar>
    </AppBar>
)

export default Header;
