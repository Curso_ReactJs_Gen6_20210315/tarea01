
export const pizzas =
    [
        {
            id: 1,
            nombre: "Pizza Napolitana",
            tamagnos: ["XL", "Familiar", "Mediana"],
            img: "napolitana.jpg",
            ingredientes: ["Salsa de Tomate", "Queso Mozzarella", "Jamón", "Tomate", "Aceitunas", "Orégano"]
        },
        {
            id: 2,
            nombre: "Pizza Azurra",
            tamagnos: ["XL", "Familiar", "Pequeña"],
            img: "azurra.jpg",
            ingredientes: ["Salsa de tomate", "Queso Mozarella", "Champiñones", "Tomate", "Cebolla", "Orégano"]
        },
        {
            id: 3,
            nombre: "Pizza Hawaiana",
            tamagnos: ["XL", "Familiar", "Mediana", "Pequeña"],
            img: "hawaiana.jpg",
            ingredientes: ["Salsa de Tomate", "Queso Mozzarella", "Piña", "Jamón", "Orégano"]
        },
        {
            id: 4,
            nombre: "Pizza Vegetariana",
            tamagnos: ["Familiar", "Mediana", "Pequeña"],
            img: "vegetariana.jpg",
            ingredientes: ["Salsa de tomate", "Tomate", "Queso Mozzarella", "Choclo", "Palmitos", "Tomates", "Champiñon", "Orégano"]
        },
        {
            id: 5,
            nombre: "Pizza Margarita",
            tamagnos: ["XL", "Mediana", "Pequeña"],
            img: "margarita.jpg",
            ingredientes: ["Salsa de Tomate", "Queso Mozzarella", "Carne", "Ajo", "Albaca", "Orégano"]
        },
        {
            id: 6,
            nombre: "Pizza Española",
            tamagnos: ["Familiar", "Mediana"],
            img: "espanola.jpg",
            ingredientes: ["Salsa de Tomate", "Queso Mozzarella", "Choricillo", "Pimentón", "Aceituna Negra", "Orégano"]
        },
        {
            id: 7,
            nombre: "Pizza Pacifico",
            tamagnos: ["XL", "Mediana", "Pequeña"],
            img: "pacifico.jpg",
            ingredientes: ["Salsa de Tomate", "Queso Mozzarella", "Camarones", "Choritos", "Ajo", "Orégano"]
        },
        {
            id: 8,
            nombre: "Pizza Calabresa",
            tamagnos: ["XL", "Familiar"],
            img: "calabresa.jpg",
            ingredientes: ["Salsa de Tomate", "Queso Mozzarella", "Salame Italiano", "Pimentón Rojo", "Aceituna Negra"]
        }
    ];

