import { Grid } from '@material-ui/core';
import React, { Fragment } from 'react'
import Footer from '../../components/Footer.component';
import Header from '../../components/Header.component';
import PizzaCard from '../../components/PizzaCard.component';
import {pizzas} from '../data';

const Homepage = () => {
    
    console.log(pizzas);
    
    return (
        <Fragment>
            <Header></Header>
            <Grid container  justify='center'>
            {
               pizzas.map(p => 
                    <Grid item xs={12} sm={4} md={3}>
                        <PizzaCard id={p.id} img={`../../assets/img/${p.img}`} nombre={p.nombre} />
                    </Grid>
               ) 
            }
            </Grid>
            <Footer></Footer>
        </Fragment>
    )
}

export default Homepage;
