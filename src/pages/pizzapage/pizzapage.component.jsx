import React, { Fragment } from 'react'
import { useParams } from 'react-router';
import Footer from '../../components/Footer.component';
import Header from '../../components/Header.component';
import PizzaDetail from '../../components/PizzaDetail.component';
import { pizzas } from '../data';

const PizzaPage = () => {

    const { idPizza } = useParams();
    const pizza = pizzas.find(p => p.id === parseInt(idPizza))

    return (
        <Fragment>
            <Header></Header>
            <PizzaDetail pizza={pizza}/>
            <Footer></Footer>
        </Fragment>
    )
}

export default PizzaPage;